from __future__ import unicode_literals
from django.views.generic import TemplateView


class DashboardMainView(TemplateView):
    template_name = 'admin/main.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)