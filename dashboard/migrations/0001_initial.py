# Generated by Django 2.2.9 on 2020-01-08 01:27

import dashboard.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PostModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=20, validators=[dashboard.models.validate_title])),
                ('body', models.TextField()),
                ('email', models.EmailField(default='nama@web.com', max_length=254)),
                ('alamat', models.CharField(blank=True, max_length=200)),
                ('category', models.CharField(choices=[('Jurnal', 'Jurnal'), ('Blog', 'Blog'), ('Berita', 'Berita')], default='Jurnal', max_length=20)),
                ('waktu_posting', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
