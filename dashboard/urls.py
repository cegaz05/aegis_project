from django.urls import path, include
from dashboard.sites import DashboardSite

admin.site = DashboardSite()
admin.autodiscover()


urlpatterns = [
    path('admin/', include(admin.site.urls)),
]
