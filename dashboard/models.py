from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
def validate_title(value):
    judul_input = value
    if judul_input == "Einstein":
        message = "maaf, " + judul_input + "tidak bisa diinput"
        raise ValidationError(message)

class PostModel(models.Model):
    title = models.CharField(
        max_length=20,
        validators = [validate_title]
    )
    body = models.TextField()
    email = models.EmailField(default="nama@web.com")
    alamat = models.CharField(max_length=200, blank=True)
    LIST_CATEGORY = (
        ('Jurnal','Jurnal'),
        ('Blog', 'Blog'),
        ('Berita', 'Berita')
    )
    category = models.CharField(
        max_length=20,
        choices = LIST_CATEGORY,
        default="Jurnal")
    waktu_posting = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_draft = models.BooleanField(default=True)

    def __str__(self):
        return "{}.{}".format(self.id, self.title)