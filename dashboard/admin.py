from django.contrib import admin
from django.forms import ModelForm, TextInput
# Register your models here.
from .models import PostModel
from suit.widgets import AutosizedTextarea, EnclosedInput
from suit.admin import RelatedFieldAdmin


class InLine(RelatedFieldAdmin):
    model = PostModel
    sortable = 'order'

class PostModelForm(ModelForm, EnclosedInput):
    class Meta:
        widgets = {
            'body': AutosizedTextarea(attrs={'rows': 3, 'class': 'input-xlarge'}),
            'email': EnclosedInput(append='Email User')
        }

class PostAdmin(admin.ModelAdmin):
    form = PostModelForm
    modelLine = InLine
    list_display = ('title','waktu_posting','updated')
    list_filter = ['waktu_posting']
    search_fields = ['title']


admin.site.site_header = 'Admin';
admin.site.site_title = 'Dashboard Admin';
admin.site.index_title = 'Menu Admin'
admin.site.register(PostModel, PostAdmin);