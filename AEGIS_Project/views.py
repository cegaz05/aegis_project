from django.shortcuts import render, redirect
from dashboard.forms import PostForm
from dashboard.models import PostModel

def index(request):
    posts = PostModel.objects.all()
    context = {
        'page_title':'List Post',
        'posts':posts,
    }
    return render(request, 'index.html', context) #untuk template

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        if post_form.is_valid():
            post_form.save()
        return redirect("/")

    context = {
        'page_title':'create post',
        'post_form':post_form,
    }

    return render(request, 'create.html', context)

def delete(request,delete_id):
    PostModel.objects.filter(id=delete_id).delete()
    return redirect("/")

def update(request,update_id):
    post_update = PostModel.objects.get(id=update_id)
    data = {
        'title':post_update.title,
        'body': post_update.body,
        'email': post_update.email,
        'alamat': post_update.alamat,
        'category': post_update.category,
    }
    post_form = PostForm(request.POST or None, initial=data, instance=post_update)
    if request.method == 'POST':
        if post_form.is_valid():
            post_form.save()
        return redirect("/")

    context = {
        'page_title':'Update Akun post',
        'post_form':post_form,
    }
    return render (request, "create.html", context)