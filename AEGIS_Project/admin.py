from django.contrib import admin
from django.contrib.auth.models import Group

# Register your models here.
from dashboard.models import PostModel


class PostAdmin(admin.ModelAdmin):
    list_display = ('title','email','waktu_posting')
    list_filter = ['waktu_posting']
    search_fields = ['title']

admin.site.site_header = 'Admin';
admin.site.site_title = 'Dashboard Admin';
admin.site.index_title = 'Menu Admin'
admin.site.register(PostModel, PostAdmin);
admin.site.unregister(Group);
